import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { style } from './styles';
import { BleManager } from 'react-native-ble-plx';


export default class Blue extends Component {
    constructor(props) {
        super(props);

        this.manager = new BleManager();
        this.state = {info: "", values: {}}
    }

    info(message) {
        this.setState({info: message})
    }
    
      error(message) {
        this.setState({info: "ERROR: " + message})
    }
    
      updateValue(key, value) {
        this.setState({values: {...this.state.values, [key]: value}})
    }

    componentWillMount() {
        if (Platform.OS === 'ios') {
            this.manager.onStateChange((state) => {
            if (state === 'PoweredOn') 
                this.scanAndConnect()
            })
        } else {
            this.scanAndConnect()
        }
    }

    scanAndConnect() {
        this.manager.startDeviceScan(null, null, (error, device) => {
            console.log("Scanning...");
            this.info("Scanning...")
            if (error) {
                this.error(error.message)
                return
            }
            if(device.name){
                if (device.name.indexOf('OneTouch') > -1) {
                    this.manager.stopDeviceScan();
                    this.info("Connecting to Glucometer "+ device.name);
                    console.log("Connecting to Glucometer", device.id);
                    this.manager.connectToDevice(device.id)
                    .then((info) => {
                        console.log("Connected");
                        //Validacion si el dispositivo esta conectado
                        this.manager.isDeviceConnected(device.id)
                        .then((connect) =>{
                            console.log('Connect: ',connect); 
                            this.getInfo(device)
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                        
                    })
                }
            }
        });
    }

    async getInfo(device) {
        console.log(device);
        
        await device.discoverAllServicesAndCharacteristics()
        const services = await device.services();
        console.log(services);
        
        services.forEach(async service => {
            const characteristics = await device.characteristicsForService(service.uuid);
            characteristics.forEach(async characteristic => {
                if(characteristic.isReadable){
                    characteristic.read()
                    .then((data)=>{
                        if(data.isReadable){
                            console.log('characteristic: ',data.value);
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                await characteristic.monitor((error, char)=>{ 
                    if(error){
                        console.log('characteristicError: ',JSON.stringify(error));
                    }else{
                        console.log('characteristic: ',char);
                    }
                })

                const descriptors = await device.descriptorsForService(characteristic.serviceUUID, characteristic.uuid)
                descriptors.forEach(async descriptor => {
                    descriptor.read()
                    .then((data)=>{
                        if(data.value){
                            console.log('descriptor: ',data.value);
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                   
                })
                
            })
        })
    }

    readAll(device){
        this.manager.discoverAllServicesAndCharacteristicsForDevice(device.id)
        .then((results) => { 
            //console.log(results);
            this.manager.servicesForDevice(device.id)
            .then((services) => { 
            console.log('services');
            console.log(services);
            //console.log(this.getValueFromBase64("AwYA5AcEDRAmCgAAeLDx"));
            //console.log(this.getValueFromBase64("AwUA5AcEBxYRFwAAW7Dx"));
            this.manager.monitorCharacteristicForDevice(
                device.id, 
                '00001801-0000-1000-8000-00805f9b34fb',
                '00002A01-0000-1000-8000-00805f9b34fb', 
                (err, characteristic) => {  
                //console.log(characteristic.value);
                console.log("Glucose: " + this.getValueFromBase64(characteristic.value));
                })
            });
        });
    }

    async getValueFromBase64(dataURI) {
        //var dataURI = "AwYA5AcEDRAmCgAAeLDx"; //120 You can use this string to test this function
        //var dataURI = "AwUA5AcEBxYRFwAAW7Dx";  //91
        var str = base64.decode(dataURI);
        var utf8 = [];
        for (var i = 0; i < str.length; i++) {
            var charcode = str.charCodeAt(i);
            if (charcode < 0x80) utf8.push(charcode);
            else if (charcode < 0x800) {
                utf8.push(0xc0 | (charcode >> 6),
                    0x80 | (charcode & 0x3f));
            }
            else if (charcode < 0xd800 || charcode >= 0xe000) {
                utf8.push(0xe0 | (charcode >> 12),
                    0x80 | ((charcode >> 6) & 0x3f),
                    0x80 | (charcode & 0x3f));
            }
            // surrogate pair
            else {
                i++;
                // UTF-16 encodes 0x10000-0x10FFFF by
                // subtracting 0x10000 and splitting the
                // 20 bits of 0x0-0xFFFFF into two halves
                charcode = 0x10000 + (((charcode & 0x3ff) << 10)
                    | (str.charCodeAt(i) & 0x3ff));
                utf8.push(0xf0 | (charcode >> 18),
                    0x80 | ((charcode >> 12) & 0x3f),
                    0x80 | ((charcode >> 6) & 0x3f),
                    0x80 | (charcode & 0x3f));
            }
        }

    return uf8
    } 

    async setupNotifications(device) {
        await device.discoverAllServicesAndCharacteristics();
        const services = await device.services();
        services.forEach(async service => {
            const characteristics = await device.characteristicsForService(service.uuid);
            console.log('Caracteristicas del servicio:',characteristics);
            this.manager.monitorCharacteristicForDevice(
                service.id, 
                service.serviceUUID,
                service.uuid, 
                (err, characteristic) => {  
                //console.log(characteristic.value);
                console.log("Glucose: " + this.getValueFromBase64(characteristic.value));
            })
            
            
        });
    }

    render(){
        return(
            <View style={style.containerComponent}>
                <Text>{this.state.info}</Text>
            </View>
        )
    }

}
