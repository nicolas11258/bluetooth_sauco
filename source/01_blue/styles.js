import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
    containerComponent:{
        width: '100%', 
        height: '100%',
        padding: 0,
        alignItems:"center" 
    },
    containerStateBtl:{
        width:'100%',
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#4993ea'
    },
    textStateBtl:{
        fontSize: 18,
        fontWeight: '400',
        color: 'white'
    },
    buttonConection:{
        backgroundColor: '#4993ea',
        padding: 10,
        borderRadius: 10
    },

    sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    },
    highlight: {
    fontWeight: '700',
    },
});
