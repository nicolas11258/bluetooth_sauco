/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView
} from 'react-native';

import {
  Router,
  Scene,
  Stack,
} from 'react-native-router-flux';
import 'react-native-gesture-handler';

import Blue from './source/01_blue/blueView.component'


const App: () => React$Node = () => {
  console.disableYellowBox = true;
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Router>
          <Stack key="mainApp">
            <Scene
            type="reset"
            key="blu"
            component={ Blue }
            title="BLueView"
            hideNavBar />
          </Stack>
        </Router>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    elevation: 2
  }
});

export default App;

